package com.test.app

import android.app.Application
import com.test.app.config.ApplicationConfiguration

import com.test.app.di.DaggerApplicationComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

class CurrencyExchangeApplication : Application(), HasAndroidInjector {

    @Inject
    lateinit var activityInjector: DispatchingAndroidInjector<Any>

    override fun onCreate() {
        super.onCreate()
        DaggerApplicationComponent.builder()
            .withApplicationContext(this)
            .withApplicationConfiguration(ApplicationConfiguration())
            .build()
            .inject(this)
    }


    override fun androidInjector(): AndroidInjector<Any>? =
        activityInjector

}