package com.test.app.domain

import com.test.app.config.ApplicationConfiguration
import com.test.app.model.model.CurrencyExchangeRatesModel
import com.test.app.model.model.UserCurrencySelectionModel
import com.test.app.model.repository.CurrencyExchangeRatesRepository
import com.test.app.presentation.model.ExchangeRateViewItemModel
import io.reactivex.Flowable
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import java.math.BigDecimal
import java.math.MathContext

class ObserveUserCurrencyExchangeRatesListChangesUseCase(
    private val repository: CurrencyExchangeRatesRepository,
    private val configuration: ApplicationConfiguration,
    private val mathContext: MathContext
) {

    fun run(): Flowable<List<ExchangeRateViewItemModel>> =
        Flowable.combineLatest(
            repository.observeCurrencyExchangeRates(configuration.baseCurrency),
            repository.observeUserCurrencySelection(),
            BiFunction<CurrencyExchangeRatesModel, UserCurrencySelectionModel, List<ExchangeRateViewItemModel>>
            { currencyExchangeRatesModel, userCurrencySelectionModel ->
                currencyExchangeRatesModel
                    .mapToExchangeRateViewItemList(userCurrencySelectionModel)
                    .andPlaceBaseCurrencyAtTheTop(userCurrencySelectionModel)
            })
            .subscribeOn(Schedulers.computation())


    private fun CurrencyExchangeRatesModel.mapToExchangeRateViewItemList(userCurrencySelectionModel: UserCurrencySelectionModel): List<ExchangeRateViewItemModel> =
        ratesMap
            .asSequence()
            .map {
                ExchangeRateViewItemModel(
                    it.key,
                    calculateCurrencyAmount(this, userCurrencySelectionModel, it.value)
                )
            }
            .plus(
                ExchangeRateViewItemModel(
                    configuration.baseCurrency,
                    calculateCurrencyAmount(this, userCurrencySelectionModel, BigDecimal("1.0"))
                )
            )
            .sortedBy { it.currency.currencyCode }
            .toList()

    private fun List<ExchangeRateViewItemModel>.andPlaceBaseCurrencyAtTheTop(
        userCurrencySelectionModel: UserCurrencySelectionModel
    ): List<ExchangeRateViewItemModel> =
        sortedByDescending { it.currency == userCurrencySelectionModel.baseCurrency }

    private fun calculateCurrencyAmount(
        currencyExchangeRatesModel: CurrencyExchangeRatesModel,
        userCurrencySelectionModel: UserCurrencySelectionModel,
        destinationCurrencyRate: BigDecimal
    ): BigDecimal =

        when (userCurrencySelectionModel.baseCurrency == configuration.baseCurrency) {
            true -> userCurrencySelectionModel.currencyAmount.multiply(destinationCurrencyRate)
            false -> userCurrencySelectionModel.currencyAmount
                .divide(
                    currencyExchangeRatesModel.ratesMap[userCurrencySelectionModel.baseCurrency],
                    mathContext
                )
                .multiply(destinationCurrencyRate, mathContext)
        }
}