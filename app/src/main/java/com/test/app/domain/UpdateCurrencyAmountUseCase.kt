package com.test.app.domain

import com.test.app.model.repository.CurrencyExchangeRatesRepository
import io.reactivex.Completable
import io.reactivex.schedulers.Schedulers
import java.math.BigDecimal

class UpdateCurrencyAmountUseCase(private val repository: CurrencyExchangeRatesRepository) {

    fun run(newCurrencyAmount: BigDecimal): Completable =
        repository
            .updateUserCurrencyAmountSelection(newCurrencyAmount)
            .subscribeOn(Schedulers.io())
}