package com.test.app.domain

import com.test.app.model.repository.CurrencyExchangeRatesRepository
import io.reactivex.Completable
import io.reactivex.schedulers.Schedulers
import java.util.*

class UpdateBaseCurrencyUseCase(private val repository: CurrencyExchangeRatesRepository) {

    fun run(newBaseCurrency: Currency): Completable =
        repository.updateUserBaseCurrencySelection(newBaseCurrency)
            .subscribeOn(Schedulers.io())
}