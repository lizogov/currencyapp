package com.test.app.di

import android.app.Application
import com.test.app.CurrencyExchangeApplication
import com.test.app.config.ApplicationConfiguration
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        RemoteServiceModule::class,
        LocalServiceModule::class,
        RepositoryModule::class,
        ActivityModule::class,
        CurrencyExchangeViewModelModule::class,
        CurrencyExchangeModule::class]
)
interface ApplicationComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun withApplicationContext(application: Application): Builder

        @BindsInstance
        fun withApplicationConfiguration(applicationConfiguration: ApplicationConfiguration): Builder

        fun build(): ApplicationComponent
    }

    fun inject(currencyExchangeApplication: CurrencyExchangeApplication)
}