package com.test.app.di

import com.test.app.config.ApplicationConfiguration
import com.test.app.model.remote.CurrencyExchangeRatesRemoteService
import com.test.app.model.remote.CurrencyRatesAPI
import com.test.app.model.remote.CurrencyRatesRemoteService
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class RemoteServiceModule {

    @Singleton
    @Provides
    fun providesCurrencyExchangeRatesRemoteService(currencyRatesAPI: CurrencyRatesAPI): CurrencyExchangeRatesRemoteService =
        CurrencyRatesRemoteService(currencyRatesAPI)

    @Singleton
    @Provides
    fun providesCurrencyExchangeRatesAPI(restAdapter: Retrofit): CurrencyRatesAPI =
        restAdapter.create(CurrencyRatesAPI::class.java)


    @Singleton
    @Provides
    fun providesRestAdapter(applicationConfiguration: ApplicationConfiguration): Retrofit =
        Retrofit.Builder()
            .baseUrl(applicationConfiguration.currencyExchangeRatesApiEndpoint)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()

    @Singleton
    @Provides
    fun providesOkHttpClient(applicationConfiguration: ApplicationConfiguration): OkHttpClient =
        OkHttpClient.Builder()
            .connectTimeout(
                applicationConfiguration.connectionTimeoutValue,
                applicationConfiguration.connectionTimeoutTimeUnit
            )
            .readTimeout(
                applicationConfiguration.readTimeoutValue,
                applicationConfiguration.readTimeoutTimeUnit
            )
            .build()
}