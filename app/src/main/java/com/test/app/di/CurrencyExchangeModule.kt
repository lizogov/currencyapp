package com.test.app.di

import com.test.app.config.ApplicationConfiguration
import com.test.app.domain.ObserveUserCurrencyExchangeRatesListChangesUseCase
import com.test.app.domain.UpdateBaseCurrencyUseCase
import com.test.app.domain.UpdateCurrencyAmountUseCase
import com.test.app.model.repository.CurrencyExchangeRatesRepository
import dagger.Module
import dagger.Provides
import java.math.MathContext
import javax.inject.Singleton

@Module
class CurrencyExchangeModule {

    @Singleton
    @Provides
    fun providesObserveUserCurrencyExchangeRatesListUseCase(
        repository: CurrencyExchangeRatesRepository,
        configuration: ApplicationConfiguration,
        mathContext: MathContext
    ): ObserveUserCurrencyExchangeRatesListChangesUseCase =
        ObserveUserCurrencyExchangeRatesListChangesUseCase(repository, configuration, mathContext)

    @Singleton
    @Provides
    fun providesChangeCurrencyAmountUseCase(repository: CurrencyExchangeRatesRepository): UpdateCurrencyAmountUseCase =
        UpdateCurrencyAmountUseCase(repository)

    @Singleton
    @Provides
    fun providesChangeBaseCurrencyUseCase(repository: CurrencyExchangeRatesRepository): UpdateBaseCurrencyUseCase =
        UpdateBaseCurrencyUseCase(repository)

    @Singleton
    @Provides
    fun providesMathContext(configuration: ApplicationConfiguration): MathContext = MathContext(
        configuration.mathCalculationPrecision,
        configuration.mathCalculationRoundingMode
    )
}