package com.test.app.di

import com.test.app.config.ApplicationConfiguration
import com.test.app.model.local.UserCurrencySelectionLocalService
import com.test.app.model.remote.CurrencyExchangeRatesRemoteService
import com.test.app.model.repository.CurrencyExchangeRatesRepository
import com.test.app.model.repository.DualCurrencyExchangeRatesRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Singleton
    @Provides
    fun providesCurrencyExchangeRatesRepository(
        localServiceUser: UserCurrencySelectionLocalService,
        remoteService: CurrencyExchangeRatesRemoteService,
        applicationConfiguration: ApplicationConfiguration
    ): CurrencyExchangeRatesRepository =
        DualCurrencyExchangeRatesRepository(
            localServiceUser,
            remoteService,
            applicationConfiguration
        )
}