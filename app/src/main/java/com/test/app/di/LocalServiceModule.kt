package com.test.app.di

import android.app.Application
import com.test.app.model.local.RoomUserCurrencySelectionDatabase
import com.test.app.model.local.RoomUserCurrencySelectionLocalService
import com.test.app.model.local.UserCurrencySelectionLocalService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class LocalServiceModule {

    @Singleton
    @Provides
    fun providesCurrencyExchangeRateLocalService(roomUserCurrencySelectionDatabase: RoomUserCurrencySelectionDatabase): UserCurrencySelectionLocalService =
        RoomUserCurrencySelectionLocalService(roomUserCurrencySelectionDatabase)

    @Singleton
    @Provides
    fun providesRoomCurrencyExchangeRatesDatabase(application: Application): RoomUserCurrencySelectionDatabase =
        RoomUserCurrencySelectionDatabase.getInstance(application)
}