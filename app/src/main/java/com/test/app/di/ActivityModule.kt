package com.test.app.di

import com.test.app.presentation.activity.CurrenciesListActivity
import dagger.Module
import dagger.android.AndroidInjector
import dagger.android.ContributesAndroidInjector

@Module
interface ActivityModule : AndroidInjector<CurrenciesListActivity> {

    @ContributesAndroidInjector
    fun contributeCurrencyExchangeListActivity(): CurrenciesListActivity
}