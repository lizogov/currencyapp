package com.test.app.model.remote

import com.test.app.model.model.CurrencyExchangeRatesModel
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import java.math.BigDecimal
import java.util.*

class CurrencyRatesRemoteService(private val currencyRatesApi: CurrencyRatesAPI) :
    CurrencyExchangeRatesRemoteService {

    override fun getExchangeRatesForBase(baseCurrency: Currency): Single<CurrencyExchangeRatesModel> =
        currencyRatesApi
            .getCurrencyRatesForBase(baseCurrency)
            .map {
                CurrencyExchangeRatesModel(
                    Currency.getInstance(it.baseCurrency),
                    it.ratesMap.map { ratesMap ->
                        Currency.getInstance(ratesMap.key) to BigDecimal(
                            ratesMap.value
                        )
                    }.toMap()
                )
            }
            .subscribeOn(Schedulers.io())
}