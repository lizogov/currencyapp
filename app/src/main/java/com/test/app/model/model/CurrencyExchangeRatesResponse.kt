package com.test.app.model.model

import com.google.gson.annotations.SerializedName

data class CurrencyExchangeRatesResponse(
    @SerializedName("baseCurrency") val baseCurrency: String,
    @SerializedName("rates") val ratesMap: Map<String, String>
)