package com.test.app.model.remote

import com.test.app.model.model.CurrencyExchangeRatesResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.*

interface CurrencyRatesAPI {

    @GET("api/android/latest")
    fun getCurrencyRatesForBase(@Query("base") baseCurrency: Currency): Single<CurrencyExchangeRatesResponse>
}